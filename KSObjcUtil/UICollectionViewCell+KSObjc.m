//
//  UICollectionViewCell+KSObjc.m
//  KSObjcUtil
//
//  Created by 孔 on 2023/8/16.
//

#import "UICollectionViewCell+KSObjc.h"

@implementation UICollectionViewCell (KSObjc)
- (UICollectionView *)collectionView{
    UIView* superview = self.superview;
    
    while (superview && ![superview isKindOfClass:[UICollectionView class]]) {
        superview = superview.superview;
    }
    
    return (UICollectionView*)superview;
}

- (NSIndexPath *)indexPath{
    return [self.collectionView indexPathForCell:self];
}

@end
