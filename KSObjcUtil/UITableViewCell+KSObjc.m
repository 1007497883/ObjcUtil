//
//  UITableViewCell+KSObjc.m
//  KSObjcUtilDemo
//
//  Created by 孔 on 2023/5/30.
//  Copyright © 2023 kong. All rights reserved.
//

#import "UITableViewCell+KSObjc.h"

@implementation UITableViewCell (KSObjc)

- (UITableView *)tableView{
    UIView* superview = self.superview;
    
    while (superview && ![superview isKindOfClass:[UITableView class]]) {
        superview = superview.superview;
    }
    
    return (UITableView*)superview;
}

- (NSIndexPath *)indexPath{
    return [self.tableView indexPathForCell:self];
}

- (void)willDisplayCorner:(UIRectCorner)corner{
    UITableView* tableView = self.tableView;
    NSIndexPath* indexPath = self.indexPath;
    
    if ([self respondsToSelector:@selector(tintColor)]) {
        // 圆角弧度半径
        CGFloat cornerRadius = 16;
        // 设置cell的背景色为透明，如果不设置这个的话，则原来的背景色不会被覆盖
//        cell.backgroundColor = UIColor.clearColor;

        // 创建一个shapeLayer
        CAShapeLayer *layer = [[CAShapeLayer alloc] init];
        CAShapeLayer *backgroundLayer = [[CAShapeLayer alloc] init]; //显示选中
        // 创建一个可变的图像Path句柄，该路径用于保存绘图信息
        CGMutablePathRef pathRef = CGPathCreateMutable();
        // 获取cell的size
        CGRect bounds = CGRectInset(self.bounds, 16, 0);
        
        NSInteger numberOfRows = [tableView numberOfRowsInSection:indexPath.section];
       
        if (indexPath.row == 0 && numberOfRows == 1) {
            CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
        }else if (indexPath.row == 0) {
            // 初始起点为cell的左下角坐标
            CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
            CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), corner & UIRectCornerTopLeft?cornerRadius:0);
            CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), corner & UIRectCornerTopRight?cornerRadius:0);
            CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
            
        } else if (indexPath.row == numberOfRows -1) {
            // 初始起点为cell的左上角坐标
            CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
            CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), corner & UIRectCornerBottomLeft?cornerRadius:0);
            CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), corner & UIRectCornerBottomRight?cornerRadius:0);
            CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
        } else {
            CGPathAddRect(pathRef, nil, bounds);
//                needSeparator = YES;
        }
//
//        // 这里要判断分组列表中的第一行，每组section的第一行，每组section的中间行
////        BOOL addLine = NO;
//        // CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
//        if (indexPath.row == 0) {
//            // 初始起点为cell的左下角坐标
//            CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
//            // 起始坐标为左下角，设为p1，（CGRectGetMinX(bounds), CGRectGetMinY(bounds)）为左上角的点，设为p1(x1,y1)，(CGRectGetMidX(bounds), CGRectGetMinY(bounds))为顶部中点的点，设为p2(x2,y2)。然后连接p1和p2为一条直线l1，连接初始点p到p1成一条直线l，则在两条直线相交处绘制弧度为r的圆角。
//            CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), corner & UIRectCornerTopLeft?cornerRadius:0);
//            CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds),  corner & UIRectCornerTopRight?cornerRadius:0);
//            // 终点坐标为右下角坐标点，把绘图信息都放到路径中去,根据这些路径就构成了一块区域了
//            CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
////            addLine = YES;
//        } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
//            // 初始起点为cell的左上角坐标
//            CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
//            CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds),  corner & UIRectCornerBottomLeft?cornerRadius:0);
//            CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds),  corner & UIRectCornerBottomRight?cornerRadius:0);
//            // 添加一条直线，终点坐标为右下角坐标点并放到路径中去
//            CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
//        } else {
//            // 添加cell的rectangle信息到path中（不包括圆角）
//            CGPathAddRect(pathRef, nil, bounds);
////            addLine = YES;
//        }
        // 把已经绘制好的可变图像路径赋值给图层，然后图层根据这图像path进行图像渲染render
        layer.path = pathRef;
        backgroundLayer.path = pathRef;
        // 注意：但凡通过Quartz2D中带有creat/copy/retain方法创建出来的值都必须要释放
        CFRelease(pathRef);
        // 按照shape layer的path填充颜色，类似于渲染render
        // layer.fillColor = [UIColor colorWithWhite:1.f alpha:0.8f].CGColor;
        layer.fillColor = [UIColor whiteColor].CGColor;
        // 添加分隔线图层
//        if (addLine == YES) {
//            CALayer *lineLayer = [[CALayer alloc] init];
//            CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
//            lineLayer.frame = CGRectMake(CGRectGetMinX(bounds), bounds.size.height-lineHeight, bounds.size.width, lineHeight);
//            // 分隔线颜色取自于原来tableview的分隔线颜色
//            lineLayer.backgroundColor = tableView.separatorColor.CGColor;
//            [layer addSublayer:lineLayer];
//        }

        // view大小与cell一致
        UIView *roundView = [[UIView alloc] initWithFrame:bounds];
        
        // 添加自定义圆角后的图层到roundView中
        [roundView.layer insertSublayer:layer atIndex:0];
        roundView.backgroundColor = UIColor.clearColor;
        //cell的背景view
        //cell.selectedBackgroundView = roundView;
        self.backgroundView = roundView;

        //以上方法存在缺陷当点击cell时还是出现cell方形效果，因此还需要添加以下方法
        UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:bounds];
        backgroundLayer.fillColor = tableView.separatorColor.CGColor;
        [selectedBackgroundView.layer insertSublayer:backgroundLayer atIndex:0];
        selectedBackgroundView.backgroundColor = UIColor.clearColor;
        self.selectedBackgroundView = selectedBackgroundView;
    }
}
@end
