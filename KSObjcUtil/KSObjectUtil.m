//
//  KSObjectUtil.m
//  KSObjcUtilDemo
//
//  Created by Mac on 2020/3/12.
//  Copyright © 2020 kong. All rights reserved.
//

#import "KSObjectUtil.h"
#import <objc/runtime.h>

@implementation KSObjectUtil

void ks_exchangeMethod(Class oriCls,SEL oriSel,Class swiCls,SEL swiSel){
    Method origMethod = class_getInstanceMethod(oriCls, oriSel);
    Method swizMethod = class_getInstanceMethod(swiCls, swiSel);
    
    BOOL didAddMethod = class_addMethod(oriCls, oriSel, method_getImplementation(swizMethod), method_getTypeEncoding(swizMethod));
    
    if (didAddMethod){
        class_replaceMethod(oriCls, swiSel, method_getImplementation(origMethod), method_getTypeEncoding(origMethod));
    }
    else{
        method_exchangeImplementations(origMethod, swizMethod);
    }
    
}

@end
