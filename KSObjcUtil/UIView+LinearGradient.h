//
//  UIView+LinearGradient.h
//  健身力量站
//
//  Created by KS on 2023/10/15.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

// 线性渐变
@interface UIView (LinearGradient)

/**
开始颜色
 */
@property (nonatomic, strong) IBInspectable UIColor* fromColor;
/**
 结束颜色
 */
@property (nonatomic, strong) IBInspectable UIColor* toColor;
/**
 开始位置
 */
@property (nonatomic, assign) IBInspectable CGPoint startPoint;
/**
 结束位置
 */
@property (nonatomic, assign) IBInspectable CGPoint endPoint;

@property (strong, nonatomic) CAGradientLayer* gradientLayer;
@end

NS_ASSUME_NONNULL_END
