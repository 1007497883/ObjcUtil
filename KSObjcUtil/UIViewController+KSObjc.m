//
//  UIViewController+KSObjc.m
//  KSObjcUtilDemo
//
//  Created by 孔 on 2023/5/30.
//  Copyright © 2023 kong. All rights reserved.
//

#import "UIViewController+KSObjc.h"
#import "KSMacro.h"

@implementation UIViewController (KSObjc)

+ (instancetype)initWithStoryboard{
    
    NSString* name = NSStringFromClass(self);

    UIViewController* controller = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:name];

    if (!controller) {
        controller = [[self alloc] init];
    }
    return controller;
}

- (void)alertTitle:(NSString*)title
           okTitle:(NSString*)okTitle
          okAction:(UIViewControllerAlertAction)okAction{
    UIAlertController* al = [UIAlertController alertControllerWithTitle:title
                                                                message:nil
                                                         preferredStyle:UIAlertControllerStyleAlert];
    [al addAction:[UIAlertAction actionWithTitle:okTitle
                                           style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * _Nonnull action) {
        okAction?okAction():NO;
    }]];
    [self presentViewController:al animated:YES completion:NULL];
}


- (void)alertTitle:(NSString*)title
           okTitle:(NSString*)okTitle
          okAction:(UIViewControllerAlertAction)okAction
           noTitle:(NSString*)noTitle{
    UIAlertController* al = [UIAlertController alertControllerWithTitle:title
                                                                message:nil
                                                         preferredStyle:UIAlertControllerStyleAlert];
    
    [al addAction:[UIAlertAction actionWithTitle:okTitle
                                           style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * _Nonnull action) {
        okAction?okAction():NO;
    }]];
    [al addAction:[UIAlertAction actionWithTitle:noTitle style:UIAlertActionStyleCancel handler:NULL]];
    [self presentViewController:al animated:YES completion:NULL];
    
}

- (void)alertTitle:(NSString*)title
           okTitle:(NSString*)okTitle
          okAction:(UIViewControllerAlertAction)okAction
          ok2Title:(NSString*)ok2Title
         ok2Action:(UIViewControllerAlertAction)ok2Action{
    
    UIAlertController* al = [UIAlertController alertControllerWithTitle:title
                                                                message:nil
                                                         preferredStyle:UIAlertControllerStyleAlert];
    [al addAction:[UIAlertAction actionWithTitle:ok2Title
                                           style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * _Nonnull action) {
        ok2Action?ok2Action():NO;
    }]];
    
    [al addAction:[UIAlertAction actionWithTitle:okTitle
                                           style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * _Nonnull action) {
        okAction?okAction():NO;
    }]];
    
    [self presentViewController:al animated:YES completion:NULL];
    
}

- (void)alertFieldTitle:(NSString*)title
                okTitle:(NSString*)okTitle
               okAction:(UIViewControllerFieldAction)okAction
               ok2Title:(NSString*)ok2Title
              ok2Action:(UIViewControllerAlertAction)ok2Action{
    
    UIAlertController* al = [UIAlertController alertControllerWithTitle:title
                                                                message:nil
                                                         preferredStyle:UIAlertControllerStyleAlert];
    @Weak(al);
    [al addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.keyboardType = UIKeyboardTypeASCIICapable;
    }];
    
    [al addAction:[UIAlertAction actionWithTitle:ok2Title
                                           style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * _Nonnull action) {
        ok2Action?ok2Action():NO;
    }]];
    
    [al addAction:[UIAlertAction actionWithTitle:okTitle
                                           style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * _Nonnull action) {
        okAction?okAction(alWeak.textFields.firstObject.text):NO;
    }]];
    
    [self presentViewController:al animated:YES completion:NULL];
    
}
@end
