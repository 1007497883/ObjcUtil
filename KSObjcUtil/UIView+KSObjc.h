//
//  UIView+KSObjc.h
//  KSObjcUtilDemo
//
//  Created by 孔 on 2023/5/30.
//  Copyright © 2023 kong. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (KSObjc)

@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;

/**
 指定圆角，默认全部
 左上角 1<<0
 左下角 1<<1
 右上角 1<<2
 右下角 1<<3
 */
@property (nonatomic, assign) IBInspectable NSUInteger maskedCorners;

@property (nonatomic, assign) IBInspectable CGFloat borderWidth;
@property (nonatomic, assign) IBInspectable UIColor* borderColor;

//从xib加载同名视图
+ (instancetype)viewFromXib;

//截图
- (UIImage*)captureRect:(CGRect)rect;

//添加点击事件
- (void)addGesture:(UIGestureRecognizer*)gesture
         withBlock:(void(^)(UIGestureRecognizer * _Nonnull recognizer))block;
- (void)addTapGestureWithBlock:(void(^)(UIGestureRecognizer * _Nonnull recognizer))tapBlock;

@end

NS_ASSUME_NONNULL_END
