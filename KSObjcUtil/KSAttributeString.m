//
//  KSAttributeString.m
//  健身力量站
//
//  Created by 孔 on 2024/4/20.
//

#import "KSAttributeString.h"
#import <CoreText/CoreText.h>
#import "KSColorUtil.h"
@implementation KSAttributeString

+ (NSAttributedString*)string:(NSString*)string{
    return [self string:string fontSize:12 fontColor:ks_hexColor(@"#666666") lineSpacing:8];
}

+ (NSAttributedString*)string:(NSString*)string fontSize:(CGFloat)fontSize fontColor:(UIColor*)fontColor lineSpacing:(CGFloat)lineSpacing{
    return [self string:string font:[UIFont systemFontOfSize:fontSize] fontColor:fontColor lineSpacing:lineSpacing];
}

+ (NSAttributedString*)string:(NSString*)string font:(UIFont*)font fontColor:(UIColor*)fontColor lineSpacing:(CGFloat)lineSpacing{
    return [[NSAttributedString alloc] initWithString:string?:@"" attributes:[self attributeWithFont:font fontColor:fontColor lineSpacing:lineSpacing]];
}

+ (NSDictionary*)attribute{
    return [self attributeWithfontSize:12 fontColor:ks_hexColor(@"#666666") lineSpacing:8];
}

+ (NSDictionary*)attributeWithfontSize:(CGFloat)fontSize fontColor:(UIColor*)fontColor lineSpacing:(CGFloat)lineSpacing{
    return [self attributeWithFont:[UIFont systemFontOfSize:fontSize] fontColor:fontColor lineSpacing:lineSpacing];
}

+ (NSDictionary*)attributeWithFont:(UIFont*)font fontColor:(UIColor*)fontColor lineSpacing:(CGFloat)lineSpacing{
    NSMutableParagraphStyle* paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = lineSpacing;
    return @{
        NSFontAttributeName:font,
        NSForegroundColorAttributeName:fontColor,
        NSParagraphStyleAttributeName:paragraphStyle,
    };
}

//获取每行的文字
+ (NSArray<NSString*>*)linesOfAttributeString:(NSAttributedString*)string inWidth:(CGFloat)width {
    CTFramesetterRef frameSetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)string);
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectMake(0, 0, width, CGFLOAT_MAX));
    CTFrameRef frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, NULL);
    NSArray* lines = (__bridge NSArray*)CTFrameGetLines(frame);
    NSMutableArray<NSString*>* strings = [NSMutableArray array];
    for (id line in lines) {
        CTLineRef lineRef = (__bridge CTLineRef)line;
        CFRange lineRange = CTLineGetStringRange(lineRef);
        NSRange range = NSMakeRange(lineRange.location, lineRange.length);
        [strings addObject:[string.string substringWithRange:range]];
    }
    
    return strings;
}

+ (CGSize)sizeWithString:(NSString*)string attribute:(NSDictionary*)attribute{
    return [self sizeWithString:string attribute:attribute containerSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
}

+ (CGSize)sizeWithString:(NSString*)string attribute:(NSDictionary*)attribute containerSize:(CGSize)containerSize{
    return [string boundingRectWithSize:containerSize
                                options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                             attributes:attribute
                                context:nil].size;

}

@end
