//
//  KSStringUtils.m
//  KSObjcUtilDemo
//
//  Created by Mac on 2020/1/6.
//  Copyright © 2020 kong. All rights reserved.
//

#import "KSStringUtils.h"
#import <CommonCrypto/CommonDigest.h>
#import <UIKit/UIKit.h>

@implementation KSStringUtils

//是否是空字符串
BOOL ks_isEmptyString(NSString* string){
    return !string || ![string isKindOfClass:NSString.class] || string.length == 0 || [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0;
}

//是否是手机号
BOOL ks_isPhoneString(NSString* string){
    if (ks_isEmptyString(string)) {
        return NO;
    }
    
    NSString* pattern = [NSString stringWithFormat:@"^%@$",PHONE_PATTERN];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pattern];
    return [predicate evaluateWithObject:string];
}

//是否是邮箱
BOOL ks_isEmailString(NSString* string){
    if (ks_isEmptyString(string)) {
        return NO;
    }
    
    NSString* pattern = [NSString stringWithFormat:@"^%@$",EMAIL_PATTERN];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",pattern];
    return [predicate evaluateWithObject:string];
}

//是否相等
BOOL ks_isEqualString(NSString* str0,NSString* str1){
    return [str0 isEqualToString:str1];
}

//获取md5字符串
NSString* ks_md5String(NSString* string){
    const char *cStr = [string UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (CC_LONG)string.length, digest );
    NSMutableString *result = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [result appendFormat:@"%02x", digest[i]];
    return result;
}

NSString* ks_firstNonull(NSString* args,...){
    va_list strs;
    NSString* str = args;
    va_start(strs, args);
    do {
        if(!ks_isEmptyString(str)){
            break;
        }
    } while (str = va_arg(strs, NSString*));
    
    va_end(strs);
    return str;
}

//计算文字占位大小
CGSize ks_strSize(NSString* string, CGFloat fontSize){
    return ks_strSizeWithContainerSize(string, CGSizeMake(MAXFLOAT, MAXFLOAT), fontSize);
}

CGFloat ks_strHeight(NSString* string, CGFloat width, CGFloat fontSize){
    return ks_strSizeWithContainerSize(string, CGSizeMake(width, MAXFLOAT), fontSize).height;
}

CGSize ks_strSizeWithContainerSize(NSString* string, CGSize containerSize, CGFloat fontSize){
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:fontSize], NSParagraphStyleAttributeName:style};
    return [string boundingRectWithSize:containerSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:dic context:nil].size;
}

//匹配所有的正则表达式
NSArray<NSTextCheckingResult* >* ks_matches(NSString* string, NSString* pattern){
    
    if (ks_isEmptyString(string)) {
        return [NSArray new];
    }
    
    NSRegularExpression* regularExpression = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                                       options:NSRegularExpressionCaseInsensitive
                                                                                         error:nil];
    NSArray<NSTextCheckingResult* >* checkingResult = [regularExpression matchesInString:string
                                                                                 options:NSMatchingReportCompletion
                                                                                   range:NSMakeRange(0, string.length)];
    return checkingResult;
}

//匹配第一个正则表达式
NSTextCheckingResult* ks_matcheFirst(NSString* string, NSString* pattern){

    if (ks_isEmptyString(string)) {
        return nil;
    }
    
    NSRegularExpression* regularExpression = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                                       options:NSRegularExpressionCaseInsensitive
                                                                                         error:nil];
    return  [regularExpression firstMatchInString:string options:NSMatchingReportCompletion range:NSMakeRange(0, string.length)];
}

@end
