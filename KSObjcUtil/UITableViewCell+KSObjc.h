//
//  UITableViewCell+KSObjc.h
//  KSObjcUtilDemo
//
//  Created by 孔 on 2023/5/30.
//  Copyright © 2023 kong. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITableViewCell (KSObjc)
@property (nonatomic, strong, readonly, nullable) UITableView* tableView;
@property (nonatomic, strong, readonly, nullable) NSIndexPath* indexPath;

- (void)willDisplayCorner:(UIRectCorner)corner;

@end

NS_ASSUME_NONNULL_END
