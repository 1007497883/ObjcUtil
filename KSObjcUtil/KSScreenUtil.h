//
//  KSScreenUtil.h
//  test
//
//  Created by Mac on 2018/12/27.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#ifndef KS_SCREEN_W
#define KS_SCREEN_W ks_screenWidth()
#endif

#ifndef KS_SCREEN_H
#define KS_SCREEN_H ks_screenHeight()
#endif

/**
 获取屏幕bounds

 @return bounds
 */
CGRect ks_screenBounds(void);


/**
 获取屏幕size

 @return size
 */
CGSize ks_screenSize(void);


/**
 获取屏幕宽度

 @return width
 */
CGFloat ks_screenWidth(void);


/**
 获取屏幕高度

 @return height
 */
CGFloat ks_screenHeight(void);


/**
 获取屏幕缩放比率

 @return scale
 */
CGFloat ks_screenScale(void);
