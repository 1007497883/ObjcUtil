//
//  KSFileUtil.h
//  KSObjcUtilDemo
//
//  Created by Mac on 2019/1/7.
//  Copyright © 2019年 kong. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 获取document路径

 @return 路径
 */
NSString* ks_documentPath(void);

/**
 获取cache路径
 @return 路径
 */
NSString* ks_cachePath(void);

/**
  获取文件大小，并且格式化
    
 @return 10M
 */
NSString* ks_fileSize(NSURL* fileUrl);

/**
 删除文件
 */
BOOL ks_removeFile(NSURL* fileUrl);
