//
//  KSBenchmark.m
//  test
//
//  Created by Mac on 2018/12/27.
//

#import "KSBenchmark.h"

CFAbsoluteTime ks_benchmark(void(^block)(void)){
    CFAbsoluteTime start = CFAbsoluteTimeGetCurrent();
    block();
    return CFAbsoluteTimeGetCurrent() - start;
}

