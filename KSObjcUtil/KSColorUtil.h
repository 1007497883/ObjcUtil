//
//  KSColorUtil.h
//  test
//
//  Created by Mac on 2018/12/27.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIColor.h>


/**
 十六进制颜色转换

 @param hexStr #ff0000 | ff0000 | 0xff0000
 @return color
 */
UIColor* ks_hexColor(NSString* hexStr);


/**
 随机生成颜色

 @return void
 */
UIColor* ks_randomColor(void);
