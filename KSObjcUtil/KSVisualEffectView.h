//
//  KSVisualEffectView.h
//  健身力量站
//
//  Created by 孔 on 2023/11/17.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KSVisualEffectView : UIVisualEffectView
/**
 控制模糊度，[0,1]
 */
@property (assign,nonatomic) IBInspectable CGFloat intensity;
@end

NS_ASSUME_NONNULL_END
