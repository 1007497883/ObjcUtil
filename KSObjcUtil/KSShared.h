//
//  KSShared.h
//  钰家家
//
//  Created by 孔 on 2023/5/31.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KSShared : NSObject<NSCopying,NSMutableCopying>
+ (instancetype)shared;

//更新所有属性值
- (instancetype)updateAllPropertys:(NSDictionary* )keyValues NS_REQUIRES_SUPER;

//同步到本地
- (void)synchronize NS_REQUIRES_SUPER;

//清空信息，包括本地数据
- (void)clear NS_REQUIRES_SUPER;

@end

NS_ASSUME_NONNULL_END
