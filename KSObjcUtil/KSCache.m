//
//  KSCache.m
//  健身力量站
//
//  Created by KS on 2023/10/13.
//

#import "KSCache.h"

@implementation KSCache
+ (id)objectForKey:(NSString*)key{
    NSUserDefaults* defaults = NSUserDefaults.standardUserDefaults;
    return [defaults objectForKey:key];
}
+ (BOOL)boolForKey:(NSString*)key{
    NSUserDefaults* defaults = NSUserDefaults.standardUserDefaults;
    return [defaults boolForKey:key];
}

+ (void)setObject:(id)obj forKey:(NSString*)key{
    NSUserDefaults* defaults = NSUserDefaults.standardUserDefaults;
    [defaults setObject:obj forKey:key];
    [defaults synchronize];
}

+ (void)setBool:(BOOL)obj forKey:(NSString*)key{
    NSUserDefaults* defaults = NSUserDefaults.standardUserDefaults;
    [defaults setBool:obj forKey:key];
    [defaults synchronize];
}

+ (NSInteger)integerForKey:(NSString*)key{
    NSUserDefaults* defaults = NSUserDefaults.standardUserDefaults;
    NSInteger value = [defaults integerForKey:key];
    return value;
}
+ (void)setInteger:(NSInteger)obj forKey:(NSString*)key{
    NSUserDefaults* defaults = NSUserDefaults.standardUserDefaults;
    [defaults setInteger:obj forKey:key];
    [defaults synchronize];
}

+ (void)removeObjectForKey:(NSString*)key{
    NSUserDefaults* defaults = NSUserDefaults.standardUserDefaults;
    [defaults removeObjectForKey:key];
    [defaults synchronize];
}

+ (void)clear{
    NSUserDefaults* defaults = NSUserDefaults.standardUserDefaults;
    [defaults.dictionaryRepresentation enumerateKeysAndObjectsUsingBlock:^(NSString * key, id  obj, BOOL * stop) {
        [defaults removeObjectForKey:key];
    }];
    [defaults synchronize];
}
@end
