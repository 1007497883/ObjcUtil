//
//  KSCache.h
//  健身力量站
//
//  Created by KS on 2023/10/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

static NSString* CACHE_TEST = @"_service_theme_index";

@interface KSCache : NSObject

+ (id)objectForKey:(NSString*)key;
+ (void)setObject:(id)obj forKey:(NSString*)key;

+ (BOOL)boolForKey:(NSString*)key;
+ (void)setBool:(BOOL)obj forKey:(NSString*)key;

+ (NSInteger)integerForKey:(NSString*)key;
+ (void)setInteger:(NSInteger)obj forKey:(NSString*)key;

+ (void)removeObjectForKey:(NSString*)key;
+ (void)clear;
@end

NS_ASSUME_NONNULL_END
