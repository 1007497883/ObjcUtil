//
//  KSBenchmark.h
//  test
//
//  Created by Mac on 2018/12/27.
//

#import <Foundation/Foundation.h>

#if DEBUG

/**
 耗时检测,在正式环境下，一定不要使用这个方法

 @param count 重复次数，计算平均值
 @param block 代码块
 @return 平均时间，纳秒(ns)
 */
uint64_t dispatch_benchmark(size_t count, void (^block)(void));


/**
 耗时检测

 @param block 代码块
 @return 平均时间,秒(s)
 */
CFAbsoluteTime ks_benchmark(void(^block)(void));
#endif

