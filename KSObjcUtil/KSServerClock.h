//
//  KSServerClock.h
//  钰家家
//
//  Created by 孔 on 2023/10/19.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^KSServerClockSecondClock)(NSDate* date);

@interface KSServerClock : NSObject
/**
 通过单例方式获取
 */
+ (instancetype)serverTime;

/**
 重新设置服务器时间,yyyy-MM-dd HH:mm:ss
 校时
 */
+ (void)resetTime:(NSDate*)time;

/**
 获取当前时间
 */
+ (NSDate*)currentTime;

//添加秒表监听，同一个监听的key保持一致 ,非主线程
+ (void)addSecondClock:(KSServerClockSecondClock)lisenter key:(NSString*)key;
//移除秒表监听,key需要和添加的保持一致
+ (void)removeSecondClock:(NSString*)key;
@end

NS_ASSUME_NONNULL_END
