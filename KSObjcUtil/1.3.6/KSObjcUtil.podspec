
Pod::Spec.new do |s|
  s.name         = "KSObjcUtil"
  s.version      = "1.3.6"
  s.summary      = "OC常用的小工具"
  s.homepage     = "https://gitee.com/1007497883/ObjcUtil"
  s.license      = "MIT"
  s.author       = { "kong" => "m18301125620@163.com" }
  s.source       = { :git => "git@gitee.com:1007497883/ObjcUtil.git", :tag => "#{s.version}" }
  s.source_files  = "KSObjcUtil/*.{h,m}"
  s.ios.deployment_target = '11.0'
  s.frameworks = 'UIKit'
  s.dependency 'MBProgressHUD','1.2.0'
  s.dependency 'PrintBeautifulLog'
  s.dependency 'MJExtension', '3.4.1'
  s.description  = <<-DESC
                    KSBenchmark
                        函数耗时检测，仅限DEBUG模式
                    KSColorUtil
                        随机生成颜色
                        十六进制颜色转换
                    UITableViewCell
                        UITableViewCell快速获取UITableView和IndexPath
                        绘制section的圆角
                    KSScreenUtil
                        获取bounds、size、with、height、scale
                    KSDateUtil
                        日期/时间戳/字符串互转
                    KSImageUtil
                        图片压缩到指定大小/尺寸
                        图片/Base64互转
                        调整图片方向
                        保存图片到相册
                    KSFileUtil
                        获取Document路径
                        获取文件夹大小
                        快速删除文件
                    KSStringUtils
                        判断字符串是否是手机/邮箱/空
                        获取md5字符串
                        正则提取
                        其他更多功能
                    KSObjectUtil
                        runtime方法交换
                    KSPlatformUtil
                        获取设备型号
                    KSVideoUtil
                        视频转为mp4格式
                        获取视频第一帧图片
                    KSProgressHUD
                        展示提示文字
                    KSShare
                        单例父类
                    DESC
end
