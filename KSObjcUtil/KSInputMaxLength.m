//
//  KSInputMaxLength.m
//  健身力量站
//
//  Created by 孔 on 2024/5/23.
//

#import "KSInputMaxLength.h"
#import <objc/runtime.h>
@implementation KSInputMaxLength

+ (void)load{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldDidChange:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textViewDidChange:)
                                                 name:UITextViewTextDidChangeNotification
                                               object: nil];
}
+ (void)textFieldDidChange:(NSNotification*)not{
    UITextField* textField = not.object;
    if(textField.maxlength){
        NSString *toBeString = textField.text;
        NSString *lang = textField.textInputMode.primaryLanguage; // 键盘输入模式
        if([lang isEqualToString:@"zh-Hans"]) { //简体中文输入，包括简体拼音，健体五笔，简体手写
            UITextRange *selectedRange = [textField markedTextRange];
            //获取高亮部分
            UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
            //没有高亮选择的字，则对已输入的文字进行字数统计和限制
            if(!position) {
                if(toBeString.length > textField.maxlength) {
                    textField.text = [toBeString substringToIndex:textField.maxlength];
                }
            } else{ //有高亮选择的字符串，则暂不对文字进行统计和限制
            
            }
        }
        else{ //中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
            
            if(toBeString.length > textField.maxlength) {
                textField.text= [toBeString substringToIndex:textField.maxlength];
                //此方法防止emoji表情被截断
                // 3月19日更新为
            }
        }
    }
}

+ (void)textViewDidChange:(NSNotification*)not{
    UITextView* textView = not.object;
    if(textView.maxlength){
        NSString *toBeString = textView.text;
        NSString *lang = textView.textInputMode.primaryLanguage; // 键盘输入模式
        if([lang isEqualToString:@"zh-Hans"]) { //简体中文输入，包括简体拼音，健体五笔，简体手写
            UITextRange *selectedRange = [textView markedTextRange];
            //获取高亮部分
            UITextPosition *position = [textView positionFromPosition:selectedRange.start offset:0];
            //没有高亮选择的字，则对已输入的文字进行字数统计和限制
            if(!position) {
                if(toBeString.length > textView.maxlength) {
                    textView.text = [toBeString substringToIndex:textView.maxlength];
                }
            } else{ //有高亮选择的字符串，则暂不对文字进行统计和限制
            
            }
        }
        else{ //中文输入法以外的直接对其统计限制即可，不考虑其他语种情况
            
            if(toBeString.length > textView.maxlength) {
                textView.text= [toBeString substringToIndex:textView.maxlength];
                //此方法防止emoji表情被截断
                // 3月19日更新为
            }
        }
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] addObserver:KSInputMaxLength.class
                                             selector:@selector(textFieldDidChange:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object: nil];
    [[NSNotificationCenter defaultCenter] addObserver:KSInputMaxLength.class
                                             selector:@selector(textViewDidChange:)
                                                 name:UITextViewTextDidChangeNotification
                                               object: nil];
}

@end

@implementation UITextField (MaxLength)
- (void)setMaxlength:(NSInteger)maxlength{
    objc_setAssociatedObject(self, @selector(maxlength), @(maxlength), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (NSInteger)maxlength{
    return [objc_getAssociatedObject(self, _cmd) integerValue];
}

@end

@implementation UITextView (MaxLength)
- (void)setMaxlength:(NSInteger)maxlength{
    objc_setAssociatedObject(self, @selector(maxlength), @(maxlength), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (NSInteger)maxlength{
    return [objc_getAssociatedObject(self, _cmd) integerValue];
}
@end
