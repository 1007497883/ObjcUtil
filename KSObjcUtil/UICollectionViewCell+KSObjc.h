//
//  UICollectionViewCell+KSObjc.h
//  KSObjcUtil
//
//  Created by 孔 on 2023/8/16.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UICollectionViewCell (KSObjc)
@property (nonatomic, strong, readonly, nullable) UICollectionView* collectionView;
@property (nonatomic, strong, readonly, nullable) NSIndexPath* indexPath;
@end

NS_ASSUME_NONNULL_END
