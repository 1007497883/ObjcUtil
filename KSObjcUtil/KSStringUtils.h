//
//  KSStringUtils.h
//  KSObjcUtilDemo
//
//  Created by Mac on 2020/1/6.
//  Copyright © 2020 kong. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

static NSString* const URL_PATTERN = @"(https?|ftp|file)://[-A-Za-z0-9+&@#/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]";
static NSString* const EMAIL_PATTERN = @"\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
static NSString* const PHONE_PATTERN = @"1\\d{10}";

@interface KSStringUtils : NSObject

//是否是空字符串
BOOL ks_isEmptyString(NSString* string);
//是否是手机号
BOOL ks_isPhoneString(NSString* string);
//是否是邮箱(兼容国外邮箱)
BOOL ks_isEmailString(NSString* string);
//是否相等
BOOL ks_isEqualString(NSString* str0,NSString* str1);
//获取md5字符串 32位小写
NSString* ks_md5String(NSString* string);
NSString* ks_firstNonull(NSString* args,...);

//计算文字占位大小
CGSize ks_strSize(NSString* string, CGFloat fontSize);
CGFloat ks_strHeight(NSString* string, CGFloat width, CGFloat fontSize);
CGSize ks_strSizeWithContainerSize(NSString* string, CGSize containerSize, CGFloat fontSize);

//匹配所有的正则表达式
NSArray<NSTextCheckingResult* >* ks_matches(NSString* string, NSString* pattern);
//匹配第一个正则表达式
NSTextCheckingResult* ks_matcheFirst(NSString* string, NSString* pattern);

@end

NS_ASSUME_NONNULL_END
