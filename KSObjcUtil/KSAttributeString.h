//
//  KSAttributeString.h
//  健身力量站
//
//  Created by 孔 on 2024/4/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KSAttributeString : NSObject
+ (NSAttributedString*)string:(NSString*)string;
+ (NSAttributedString*)string:(NSString*)string fontSize:(CGFloat)fontSize fontColor:(UIColor*)fontColor lineSpacing:(CGFloat)lineSpacing;
+ (NSAttributedString*)string:(NSString*)string font:(UIFont*)font fontColor:(UIColor*)fontColor lineSpacing:(CGFloat)lineSpacing;

+ (NSDictionary*)attribute;
+ (NSDictionary*)attributeWithfontSize:(CGFloat)fontSize fontColor:(UIColor*)fontColor lineSpacing:(CGFloat)lineSpacing;
+ (NSDictionary*)attributeWithFont:(UIFont*)font fontColor:(UIColor*)fontColor lineSpacing:(CGFloat)lineSpacing;

+ (NSArray<NSString*>*)linesOfAttributeString:(NSAttributedString*)string inWidth:(CGFloat)width;

+ (CGSize)sizeWithString:(NSString*)string attribute:(NSDictionary*)attribute;
+ (CGSize)sizeWithString:(NSString*)string attribute:(NSDictionary*)attribute containerSize:(CGSize)containerSize;

@end

NS_ASSUME_NONNULL_END
