//
//  KSPlatformUtil.h
//  KSObjcUtilDemo
//
//  Created by Mac on 2020/5/7.
//  Copyright © 2020 kong. All rights reserved.
//  see :https://www.theiphonewiki.com/wiki/Models

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSUInteger, KSPlatformType) {
    KSPlatformUnknow = -2,
    KSPlatformSimulator = -1,
    KSPlatform2G = 0,
    KSPlatform3G,
    KSPlatform3GS,
    KSPlatform4,
    KSPlatform4S,
    KSPlatform5,
    KSPlatform5C,
    KSPlatform5S,
    KSPlatform6,
    KSPlatform6P,
    KSPlatform6S,
    KSPlatform6SP,
    
    KSPlatform7,
    KSPlatform7P,
    KSPlatform8,
    KSPlatform8P,
    
    KSPlatformX,
    KSPlatformXR,
    KSPlatformXS,
    KSPlatformXSMax,
    
    KSPlatform11,
    KSPlatform11Pro,
    KSPlatform11ProMax,
    
    KSPlatformSE,
    KSPlatformSE2,
    KSPlatformSE3,
    
    KSPlatform12Mini,
    KSPlatform12,
    KSPlatform12Pro,
    KSPlatform12ProMax,
    
    KSPlatform13Mini,
    KSPlatform13,
    KSPlatform13Pro,
    KSPlatform13ProMax,
    
    
    KSPlatform14,
    KSPlatform14Plus,
    KSPlatform14Pro,
    KSPlatform14ProMax,
    
    KSPlatformPad = 100,
    KSPlatformPad2,
    KSPlatformPad3,
    KSPlatformPad4,
    KSPlatformPad5,
    KSPlatformPad6,
    KSPlatformPad7,
    KSPlatformPad8,
    KSPlatformPad9,
    
    KSPlatformPadAir,
    KSPlatformPadAir2,
    KSPlatformPadAir3,
    KSPlatformPadAir4,
    KSPlatformPadAir5,
    
    KSPlatformPadPro129,
    KSPlatformPadPro1292,
    KSPlatformPadPro1293,
    KSPlatformPadPro1294,
    KSPlatformPadPro1295,
    KSPlatformPadPro97,
    KSPlatformPadPro105,
    KSPlatformPadPro110,
    KSPlatformPadPro1102,
    KSPlatformPadPro1103,
    
    KSPlatformPadMini,
    KSPlatformPadMini2,
    KSPlatformPadMini3,
    KSPlatformPadMini4,
    KSPlatformPadMini5,
    KSPlatformPadMini6,
    
    KSPlatformAirPods1 = 200,
    KSPlatformAirPods2,
    KSPlatformAirPodsPro,
    
    KSPlatformTV2 = 300,
    KSPlatformTV3,
    KSPlatformTV4,
    KSPlatformTV4K,
    KSPlatformTV4K2,
    
    KSPlatformWatch1 = 400,
    KSPlatformWatchS1,
    KSPlatformWatchS2,
    KSPlatformWatchS3,
    KSPlatformWatchS4,
    KSPlatformWatchS5,
    KSPlatformWatchSE,
    KSPlatformWatchS6,
    KSPlatformWatchS7,
    KSPlatformWatchSE2,
    KSPlatformWatchS8,
    KSPlatformWatchUltra,
    
    KSPlatformHomePod = 500,
    KSPlatformHomePodMini,
    
    KSPlatformPodTouch = 600,
    KSPlatformPodTouch2,
    KSPlatformPodTouch3,
    KSPlatformPodTouch4,
    KSPlatformPodTouch5,
    KSPlatformPodTouch6,
    KSPlatformPodTouch7,
};

@interface KSPlatformUtil : NSObject

KSPlatformType ks_platform(void);

@end

NS_ASSUME_NONNULL_END
