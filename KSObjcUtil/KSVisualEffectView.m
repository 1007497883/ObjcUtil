//
//  KSVisualEffectView.m
//  健身力量站
//
//  Created by 孔 on 2023/11/17.
//

#import "KSVisualEffectView.h"

@interface KSVisualEffectView ()
@property (strong,nonatomic) UIViewPropertyAnimator* animator;
@end
@implementation KSVisualEffectView

- (void)setIntensity:(CGFloat)intensity{
    _intensity = intensity;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    UIVisualEffect* effect = self.effect;
    self.effect = nil;
    if(self.animator){
        [self.animator stopAnimation:YES];
    }
    __block typeof(self) selfWeak = self;
    self.animator = [[UIViewPropertyAnimator alloc] initWithDuration:1
                                                               curve:UIViewAnimationCurveLinear
                                                          animations:^{
        selfWeak.effect = effect;
    }];
    self.animator.fractionComplete = self.intensity;
}

@end
