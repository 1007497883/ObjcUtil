//
//  KSObjectUtil.h
//  KSObjcUtilDemo
//
//  Created by Mac on 2020/3/12.
//  Copyright © 2020 kong. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KSObjectUtil : NSObject

void ks_exchangeMethod(Class oriCls,SEL oriSel,Class swiCls,SEL swiSel);

@end

NS_ASSUME_NONNULL_END
