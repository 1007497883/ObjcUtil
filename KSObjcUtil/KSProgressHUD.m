//
//  KSProgressHUD.m
//  TanPiano
//
//  Created by Mac on 2020/1/6.
//  Copyright © 2020 kong. All rights reserved.
//

#import "KSProgressHUD.h"

@implementation KSProgressHUD
+ (void)showText:(NSString *)text{
    [KSProgressHUD showText:text afterDelay:2];
}

+ (void)showText:(NSString *)text afterDelay:(NSTimeInterval)afterDelay{
    UIWindow* window = [UIApplication sharedApplication].delegate.window;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.detailsLabel.text = text;
    hud.detailsLabel.font = [UIFont systemFontOfSize:14];
    [hud showAnimated:YES];
    dispatch_async(dispatch_get_main_queue(), ^{
        [hud hideAnimated:YES afterDelay:afterDelay];
    });
}


+ (MBProgressHUD *)showLoading{
    UIWindow* window = [UIApplication sharedApplication].delegate.window;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    [hud showAnimated:YES];
    return hud;
}

+ (MBProgressHUD *)showLoading:(NSString *)text{
    UIWindow* window = [UIApplication sharedApplication].delegate.window;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    hud.label.text = text;
    [hud showAnimated:YES];
    return hud;
}

+ (void)hideLoading:(MBProgressHUD *)hud{
    dispatch_async(dispatch_get_main_queue(), ^{
        [hud hideAnimated:YES];
    });
}
@end
