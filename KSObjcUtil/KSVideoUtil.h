//
//  KSVideoUtil.h
//  KSObjcUtilDemo
//
//  Created by Mac on 2020/5/14.
//  Copyright © 2020 kong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <AVFoundation/AVAssetExportSession.h>
#import <UIKit/UIImage.h>

typedef void(^KSVideoTransformComplete)(NSURL* fileUrl,NSError* error,AVAssetExportSessionStatus status);
typedef void(^KSVideoFirstFrameComplete)(UIImage* image);

@interface KSVideoUtil : NSObject

//视频转为mp4
void ks_videoToMp4(NSURL* source,KSVideoTransformComplete complete);

//获取视频第一帧
void ks_firstFrame(NSURL* source, CGSize size, KSVideoFirstFrameComplete complete);

@end
