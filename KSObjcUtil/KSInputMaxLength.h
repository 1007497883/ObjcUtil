//
//  KSInputMaxLength.h
//  健身力量站
//
//  Created by 孔 on 2024/5/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface KSInputMaxLength : NSObject

@end

@interface UITextField (MaxLength)
@property (nonatomic, assign) IBInspectable NSInteger maxlength;
@end

@interface UITextView (MaxLength)
@property (nonatomic, assign) IBInspectable NSInteger maxlength;

@end
NS_ASSUME_NONNULL_END
