//
//  KSColorUtil.m
//  test
//
//  Created by Mac on 2018/12/27.
//

#import "KSColorUtil.h"

//#ff0000/#ff0000ff/#abc/ff0000/ff0000ff/abc
UIColor* ks_hexColor(NSString* hexStr){

    NSString *hex = [NSString stringWithString:hexStr];
    if ([hex hasPrefix:@"#"]) {
        hex = [hex substringFromIndex:1];
    }
    if(hex.length == 3){
        hex = [NSString stringWithFormat:@"%c%c%c%c%c%c",[hex characterAtIndex:0],
                    [hex characterAtIndex:0],
                    [hex characterAtIndex:1],
                    [hex characterAtIndex:1],
                    [hex characterAtIndex:2],
                    [hex characterAtIndex:2]];
    }
    if (hex.length == 6) {
        hex = [hex stringByAppendingString:@"FF"];
    }
    
    if (hex.length != 8){
        return nil;
    }

    uint32_t rgba;
    NSScanner *scanner = [NSScanner scannerWithString:hex];
    [scanner scanHexInt:&rgba];
    return [UIColor colorWithRed:((rgba >> 24)&0xFF) / 255.
                           green:((rgba >> 16)&0xFF) / 255.
                            blue:((rgba >> 8)&0xFF) / 255.
                           alpha:(rgba&0xFF) / 255.];

}

UIColor* ks_randomColor(void){
    
    double max = 255.0;
    
    double r = arc4random_uniform(max) / max;
    double g = arc4random_uniform(max) / max;
    double b = arc4random_uniform(max) / max;

    return [UIColor colorWithRed:r green:g blue:b alpha:1];
}

