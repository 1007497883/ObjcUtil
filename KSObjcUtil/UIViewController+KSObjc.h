//
//  UIViewController+KSObjc.h
//  KSObjcUtilDemo
//
//  Created by 孔 on 2023/5/30.
//  Copyright © 2023 kong. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^UIViewControllerAlertAction)(void);
typedef void(^UIViewControllerFieldAction)(NSString* text);


@interface UIViewController (KSObjc)
//从storyboard加载同名控制器
+ (instancetype)initWithStoryboard;

//一个一般按钮
- (void)alertTitle:(NSString*)title
           okTitle:(NSString*)okTitle
          okAction:(UIViewControllerAlertAction)okAction;

//一个一般按钮，一个取消按钮
- (void)alertTitle:(NSString*)title
           okTitle:(NSString*)okTitle
          okAction:(UIViewControllerAlertAction)okAction
           noTitle:(NSString*)noTitle;

//两个一般按钮
- (void)alertTitle:(NSString*)title
           okTitle:(NSString*)okTitle
          okAction:(UIViewControllerAlertAction)okAction
          ok2Title:(NSString*)ok2Title
         ok2Action:(UIViewControllerAlertAction)ok2Action;

//两个一般按钮,一个输入框
- (void)alertFieldTitle:(NSString*)title
                okTitle:(NSString*)okTitle
               okAction:(UIViewControllerFieldAction)okAction
               ok2Title:(NSString*)ok2Title
              ok2Action:(UIViewControllerAlertAction)ok2Action;

@end

NS_ASSUME_NONNULL_END
