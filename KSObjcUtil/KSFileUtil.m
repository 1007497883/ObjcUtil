//
//  KSFileUtil.m
//  KSObjcUtilDemo
//
//  Created by Mac on 2019/1/7.
//  Copyright © 2019年 kong. All rights reserved.
//

#import "KSFileUtil.h"

NSString* ks_documentPath(void){
    return NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
}

NSString* ks_cachePath(void){
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
}

/**
  获取文件大小，并且格式化
    
 @return 10M
 */
NSString* ks_fileSize(NSURL* fileUrl){
    NSNumber *size;
    
    [fileUrl getResourceValue:&size forKey:NSURLFileSizeKey error:nil];
    
    NSString* sourceSize = [NSByteCountFormatter stringFromByteCount:[size longLongValue]
                                                          countStyle:NSByteCountFormatterCountStyleBinary];
    
    return sourceSize;
}

/**
 删除文件
 */
BOOL ks_removeFile(NSURL* fileUrl){
    NSFileManager* fileManager = [NSFileManager defaultManager];
    
    return [fileManager removeItemAtURL:fileUrl error:nil];

}
