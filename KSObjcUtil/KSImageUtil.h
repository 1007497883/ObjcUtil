//
//  KSImageUtil.h
//  test
//
//  Created by Mac on 2018/12/29.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString * const PhotoAlbumToolErrorDomain;

typedef NS_ENUM(NSInteger, PhotoAlbumToolErrorCode) {
    PhotoAlbumToolNotAuthorized
};
/**
 批量压缩图片回调
 
 @param imgs 压缩后图片的集合
 */
typedef void(^CompressImageComplete)(NSArray<NSData*>* imgs);
typedef void(^SaveImageComplete)(BOOL success, NSError * error);

@interface KSImageUtil : NSObject


/**
 修正图片方向,解决旋转90°的问题
 
 @param src 原图
 @return 修正后的图片
 */
UIImage* ks_imageDirection(UIImage* src);


/**
 同步压缩图片质量,有误差,并且有一定限制
 
 @param src 原图
 @param byte 压缩后的大小，byte
 @return 压缩后的图片流
 */
NSData* ks_imageCompressQuality(UIImage* src, NSUInteger byte);


/**
 同步压缩尺寸,有误差
 
 @param src 原图
 @param byte 压缩后的大小，byte
 @return 压缩后的图片
 */
NSData* ks_imageCompressSize(UIImage* src ,NSUInteger byte);


/**
 异步批量压缩图片质量
 
 @param srcs 图片集合
 @param byte 限制大小
 @param complete 成功回调
 */
void ks_imageAsynCompressQuality(NSArray<UIImage*>* srcs,
                                 NSUInteger byte,
                                 CompressImageComplete complete);


/**
 同步批量压缩图片质量
 
 @param srcs 图片集合
 @param byte 限制大小
 @return 返回数组
 */
NSArray<NSData*>* ks_imageSynCompressQuality(NSArray<UIImage*>* srcs,
                                             NSUInteger byte);


/**
 异步批量压缩图片尺寸
 
 @param srcs 图片集合
 @param byte 限制大小
 @param complete 成功回调,非主线程
 */
void ks_imageAsynCompressSize(NSArray<UIImage*>* srcs,
                              NSUInteger byte,
                              CompressImageComplete complete);


/**
 同步压缩图片尺寸
 
 @param srcs 图片集合
 @param byte 限制大小
 @return 返回数组
 */
NSArray<NSData*>* ks_imageSynCompressSize(NSArray<UIImage*>* srcs,
                                          NSUInteger byte);


/**
 建议使用此方法,异步批量压缩图片质量和尺寸
 
 @param srcs 图片集合
 @param byte 限制大小单位byte
 @param complete 成功回调,非主线程
 */
void ks_imageAsynCompress(NSArray<UIImage*>* srcs,
                          NSUInteger byte,
                          CompressImageComplete complete);



/**
 同步批量压缩图片和尺寸
 
 @param srcs 图片集合
 @param byte 限制大小
 @return 返回数组
 */
NSArray<NSData*>* ks_imageSynCompress(NSArray<UIImage*>* srcs,
                                      NSUInteger byte);


/**
 修改图片大小
 
 @param src 原图
 @param size size
 @return 修改后的图片
 */
UIImage* ks_imageResize(UIImage* src, CGSize size);


/**
 从image转成base64字符串

 @param src 图片
 @return base64String
 */
NSString* ks_imageToBase64(UIImage* src);


/**
 从base64字符串转换UIImage

 @param base64 字符串
 @return UIImage
 */
UIImage* ks_imageFromBase64(NSString* base64);


/**
 保存图片到相册
 */
void ks_saveAssetToAlbum(NSURL* assetURL,SaveImageComplete completion);

/**
 保存二进制图片到相册
 */
void ks_saveDataToAlbum(NSData* data,SaveImageComplete completion);

@end

NS_ASSUME_NONNULL_END
