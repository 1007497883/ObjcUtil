//
//  KSProgressHUD.h
//  TanPiano
//
//  Created by Mac on 2020/1/6.
//  Copyright © 2020 kong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MBProgressHUD.h>

NS_ASSUME_NONNULL_BEGIN

@interface KSProgressHUD : NSObject

+ (void)showText:(NSString*)text;
+ (void)showText:(NSString*)text afterDelay:(NSTimeInterval)afterDelay;

+ (MBProgressHUD*)showLoading;
+ (MBProgressHUD*)showLoading:(NSString*)text;
+ (void)hideLoading:(MBProgressHUD*)hud;

@end

NS_ASSUME_NONNULL_END
