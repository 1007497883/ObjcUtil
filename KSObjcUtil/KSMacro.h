//
//  KSMacro.h
//  KSObjcUtilDemo
//
//  Created by 孔 on 2023/5/30.
//  Copyright © 2023 kong. All rights reserved.
//

#ifndef KSMacro_h
#define KSMacro_h

#ifdef DEBUG
#define NSLog(format, ...) {\
NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];\
[dateFormatter setDateStyle:NSDateFormatterMediumStyle];\
[dateFormatter setTimeStyle:NSDateFormatterShortStyle];\
NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];\
[dateFormatter setTimeZone:timeZone];\
[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];\
NSString *time = [dateFormatter stringFromDate:[NSDate date]];\
NSString *bundleName = NSBundle.mainBundle.infoDictionary[@"CFBundleName"];\
printf("【%s %s】 class: <%s:(%d) %p> method: %s \n%s\n",bundleName.UTF8String, time.UTF8String, [[NSString stringWithUTF8String:__FILE__].lastPathComponent UTF8String],__LINE__, self,__PRETTY_FUNCTION__, [NSString stringWithFormat:(format), ##__VA_ARGS__].UTF8String );\
}
#else
#define NSLog(...) {}
#endif


#define Weak(obj) autoreleasepool{} __weak typeof(obj) obj##Weak = obj
#define Strong(obj) autoreleasepool{} __strong typeof(obj) obj = obj##Weak

#ifndef MainDelegate
#define MainDelegate ((AppDelegate*)[UIApplication sharedApplication].delegate)
#endif

#ifndef NotificationCenter
#define NotificationCenter [NSNotificationCenter defaultCenter]
#endif

//状态栏高度
#ifndef StatusBarHeight
#define StatusBarHeight CGRectGetHeight(UIApplication.sharedApplication.statusBarFrame)
#endif

//导航栏高度
#ifndef NavigationBarHeight
#define NavigationBarHeight CGRectGetHeight(self.navigationController.navigationBar.frame)
#endif


#endif /* KSMacro_h */
