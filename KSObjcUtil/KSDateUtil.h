//
//  KSDateUtil.h
//  test
//
//  Created by Mac on 2018/12/27.
//

#import <Foundation/Foundation.h>

typedef NSString* KSDateFormat;

/**
 yyyy
 */
static KSDateFormat KSYearFormat = @"yyyy";
/**
 MM
 */
static KSDateFormat KSMonthFormat = @"MM";
/**
 dd
 */
static KSDateFormat KSDayFormat = @"dd";
/**
 HH
 */
static KSDateFormat KSHourFormat = @"HH";
/**
 mm
 */
static KSDateFormat KSMinuteFormat = @"mm";
/**
 ss
 */
static KSDateFormat KSSecondFormat = @"ss";
/**
 yyyy-MM-dd"
 */
static KSDateFormat KSShortFormat = @"yyyy-MM-dd";
/**
 HH:mm:ss
 */
static KSDateFormat KSTimeFormat = @"HH:mm:ss";
/**
 yyyy-MM-dd HH:mm:ss
 */
static KSDateFormat KSLongFormat = @"yyyy-MM-dd HH:mm:ss";
/**
 yyyy-MM-dd HH:mm
 */
static KSDateFormat KSNoSecondFormat = @"yyyy-MM-dd HH:mm";


//////////////////////////////String<->Date//////////////////////////////
/**
 日期转字符串

 @param date 日期
 @param dateFormat 日期格式,可以自定义格式
 @param timeZone 时区，nil为默认时区
 @return 时间字符串
 */
NSString* ks_strFromDate(NSDate* date,KSDateFormat dateFormat,NSTimeZone* timeZone);


/**
 字符串转日期

 @param str 格式化字符串
 @param dateFormat 字符串格式
 @param timeZone 时区，nil为默认时区
 @return date
 */
NSDate* ks_dateFromStr(NSString* str,KSDateFormat dateFormat,NSTimeZone* timeZone);


//////////////////////////////Second<->Date//////////////////////////////
/**
 现在时间戳

 @return 从1970年开始的秒数
 */
NSTimeInterval ks_secondFromNow(void);

/**
 日期转时间戳

 @param date 日期
 @return 距离1970年的秒数
 */
NSTimeInterval ks_secondFromDate(NSDate* date);

/**
 时间戳转日期

 @param second 距离1970年的秒数
 @return 日期
 */
NSDate* ks_dateFromSecond(NSTimeInterval second);


//////////////////////////////Millisecond<->Date//////////////////////////////

/**
 现在时间戳
 
 @return 从1970年开始的毫秒数
 */
NSTimeInterval ks_msecFromNow(void);

/**
 日期转时间戳
 
 @param date 日期
 @return 距离1970年的毫秒数
 */
NSTimeInterval ks_msecFromDate(NSDate* date);

/**
 时间戳转日期
 
 @param msec 距离1970年的毫秒数
 @return 日期
 */
NSDate* ks_dateFromMsec(NSTimeInterval msec);


//////////////////////////////Second<->String//////////////////////////////


/**
 字符串转时间戳

 @param str 日期字符串
 @param dateFormat 日期字符串格式
 @param timeZone 时区，nil为默认时区
 @return 距离1970年的秒数
 */
NSTimeInterval ks_secondFromStr(NSString* str,KSDateFormat dateFormat,NSTimeZone* timeZone);


/**
 时间戳转字符串

 @param second 距离1970年秒数
 @param dateFormat 日期字符串格式
 @param timeZone 时区，nil为默认时区
 @return 日期字符串
 */
NSString* ks_strFromSecond(NSTimeInterval second,KSDateFormat dateFormat,NSTimeZone* timeZone);


//////////////////////////////Millisecond<->String//////////////////////////////


/**
 字符串转时间戳
 
 @param str 日期字符串
 @param dateFormat 日期字符串格式
 @param timeZone 时区，nil为默认时区
 @return 距离1970年的毫秒数
 */
NSTimeInterval ks_msecFromStr(NSString* str,KSDateFormat dateFormat,NSTimeZone* timeZone);


/**
 时间戳转字符串
 
 @param msec 距离1970年的毫秒数
 @param dateFormat 日期字符串格式
 @param timeZone 时区，nil为默认时区
 @return 日期字符串
 */
NSString* ks_strFromMsec(NSTimeInterval msec,KSDateFormat dateFormat,NSTimeZone* timeZone);


/**
 获取单例formatter

 @return NSDateFormatter
 */
NSDateFormatter* ks_dateFormatter(void);
