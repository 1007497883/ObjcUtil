//
//  KSScreenUtil.m
//  test
//
//  Created by Mac on 2018/12/27.
//

#import "KSScreenUtil.h"

CGRect ks_screenBounds(void){
    return [UIScreen mainScreen].bounds;
}

CGSize ks_screenSize(void){
    return ks_screenBounds().size;
}

CGFloat ks_screenWidth(void){
    return ks_screenSize().width;
}

CGFloat ks_screenHeight(void){
    return ks_screenSize().height;
}

CGFloat ks_screenScale(void){
    return [UIScreen mainScreen].scale;
}
