//
//  UIView+GradientBorder.h
//  健身力量站
//
//  Created by 孔 on 2023/11/14.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
//渐变边框,UIScrollView系列不支持
@interface UIView (GradientBorder)

/**
 渐变边框粗细
 */
@property (nonatomic, assign) IBInspectable CGFloat gradientBorderWidth;
/**
 渐变开始颜色
 */
@property (nonatomic, strong) IBInspectable UIColor* borderFromColor;
/**
 渐变结束颜色
 */
@property (nonatomic, strong) IBInspectable UIColor* borderToColor;
/**
 渐变开始位置
 */
@property (nonatomic, assign) IBInspectable CGPoint borderStartPoint;
/**
 渐变结束位置
 */
@property (nonatomic, assign) IBInspectable CGPoint borderEndPoint;

@end

NS_ASSUME_NONNULL_END
