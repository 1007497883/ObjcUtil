//
//  UIView+LinearGradient.m
//  健身力量站
//
//  Created by KS on 2023/10/15.
//

#import "UIView+LinearGradient.h"
#import <objc/runtime.h>
#import "KSObjectUtil.h"

#ifndef INVALID_POINT
#define INVALID_POINT CGPointMake(-1, -1)
#endif
@implementation UIView (LinearGradient)

+ (void)load{
    ks_exchangeMethod(self, @selector(layoutSubviews), self, @selector(gradient_layoutSubviews));
    ks_exchangeMethod(self, @selector(updateConstraints), self, @selector(gradient_updateConstraints));
}

- (void)gradient_layoutSubviews{
    [self gradient_layoutSubviews];
    [self gradient_updateFrame];
}

- (void)gradient_updateConstraints{
    [self gradient_updateConstraints];
    [self gradient_updateFrame];
}

- (void)gradient_updateFrame{
    if(![self gradient_isEnableGradient]){
        return;
    }
    if(!self.gradientLayer.superlayer){
        
        [self.layer insertSublayer:self.gradientLayer atIndex:0];
    }
    
    self.gradientLayer.zPosition = -1;
    self.gradientLayer.frame = self.bounds;
    [self.gradientLayer setNeedsDisplay];
    
    //此处需要根据相应的类，存放不同的位置
//    if([self isMemberOfClass:UIView.class]){
//
//    }
}

- (BOOL)gradient_isEnableGradient{
    //同时满足四个条件才可以执行渐变
    return self.fromColor &&
    self.toColor &&
    !CGPointEqualToPoint(self.startPoint, INVALID_POINT) &&
    !CGPointEqualToPoint(self.endPoint, INVALID_POINT);
}

- (void)setFromColor:(UIColor *)fromColor{
    objc_setAssociatedObject(self, @selector(fromColor), fromColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (UIColor *)fromColor{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setToColor:(UIColor *)toColor{
    objc_setAssociatedObject(self, @selector(toColor), toColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (UIColor *)toColor{
    return objc_getAssociatedObject(self, _cmd);
}

- (void)setStartPoint:(CGPoint)startPoint{
    objc_setAssociatedObject(self, @selector(startPoint), [NSValue valueWithCGPoint:startPoint], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (CGPoint)startPoint{
    id obj = objc_getAssociatedObject(self, _cmd);
    return obj ? [obj CGPointValue]:INVALID_POINT;
}

- (void)setEndPoint:(CGPoint)endPoint{
    objc_setAssociatedObject(self, @selector(endPoint), [NSValue valueWithCGPoint:endPoint], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (CGPoint)endPoint{
    id obj = objc_getAssociatedObject(self, _cmd);
    return obj ? [obj CGPointValue]:INVALID_POINT;
}

- (void)setGradientLayer:(CAGradientLayer *)gradientLayer{
    objc_setAssociatedObject(self, @selector(gradientLayer), gradientLayer, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (CAGradientLayer *)gradientLayer{
    CAGradientLayer* _gradientLayer = objc_getAssociatedObject(self, _cmd);
    if(!_gradientLayer){
        _gradientLayer = [CAGradientLayer layer];
        [self setGradientLayer:_gradientLayer];
    }
    
    _gradientLayer.locations = @[@0,@1];
    if(!CGPointEqualToPoint(self.startPoint, INVALID_POINT)){
        _gradientLayer.startPoint = self.startPoint;
    }
    if(!CGPointEqualToPoint(self.endPoint, INVALID_POINT)){
        _gradientLayer.endPoint = self.endPoint;
    }
    
    if(self.fromColor && self.toColor){
        _gradientLayer.colors = @[(__bridge id)self.fromColor.CGColor, (__bridge id)self.toColor.CGColor];
    }
    
    return _gradientLayer;
}
@end
