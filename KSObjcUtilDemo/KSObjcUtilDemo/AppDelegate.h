//
//  AppDelegate.h
//  KSObjcUtilDemo
//
//  Created by Mac on 2019/1/4.
//  Copyright © 2019年 kong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

