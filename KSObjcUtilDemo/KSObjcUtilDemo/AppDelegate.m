//
//  AppDelegate.m
//  KSObjcUtilDemo
//
//  Created by Mac on 2019/1/4.
//  Copyright © 2019年 kong. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "KSObjcUtil.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

//验证仓库
//pod lib lint KSObjcUtil.podspec --allow-warnings --use-libraries
//发布私有仓库
//pod repo push KSObjcUtil KSObjcUtil.podspec --allow-warnings --use-libraries

//发远程有仓库
//pod trunk register m18301125620@163.com 'kong'
//pod trunk push KSObjcUtil.podspec --allow-warnings --use-libraries

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:ks_screenBounds()];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = [ViewController new];
    [self.window makeKeyAndVisible];
    return YES;
}

@end
